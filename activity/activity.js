//Actvity:

//3

db.hotel.insertOne({
    "name": "single",

    "accommodates": 2,

    "price": 1000,

    "description": "A simple room with all basic necessities",

    "roomsAvailable": 10,

    "isAvailable": false

});


//4

db.hotel.insertMany([{
        "name": "double",

        "accommodates": 3,

        "price": 2000,

        "description": "A room fit for a small family going on a vacation",

        "roomsAvailable": 5
    },

    {
        "name": "queen",

        "accommodates": 4,

        "price": 4000,

        "description": "A room with a queen sized bed perfect for a simple getaway",

        "roomsAvailable": 15,

        "isAvailable": false
    }


]);

//5

db.hotel.find({
    "name": "double"
});

//6

db.hotel.updateOne({
        "name": "queen"
    }, {
        $set: {
            "roomsAvailable": 0
        }
    }



);
//7

db.hotel.deleteMany({
    "roomsAvailable": 0
});